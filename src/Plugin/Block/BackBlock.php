<?php

namespace Drupal\back2views\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Provides a block with a back button/link
 *
 * @Block(
 *   id = "back2views_block",
 *   admin_label = @Translation("Back to Views block"),
 * )
 */
class BackBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {

    // Getting the confitured values.
    $config = $this->getConfiguration();
    $target_path = $config['back2views']['back_btn']['target_path'];
    $target_title = $config['back2views']['back_btn']['target_link_title'];
    $entity_type = $config['back2views']['add_new']['entity_type'] ?? FALSE;
    $bundle = $config['back2views']['add_new']['bundle'] ?? FALSE;

    // Getting the referer.
    $request = \Drupal::request();
    $referer = $request->headers->get('referer');

    // Getting the base url.
    $base_url = Request::createFromGlobals()->getSchemeAndHttpHost();

    // When the target is found in the referer
    if (strpos($referer, $target_path) !== FALSE) {
      // Getting the alias or the relative path.
      $referer_path = substr($referer, strlen($base_url));
    }
    else {
      $referer_path = '/'.$target_path;
    }
    
    $data = [
              'referrer_path' => $referer_path,
              'target_path' => $target_path,
              'target_title' => $target_title,
            ];

    $is_accessible_to_create = FALSE;
    if ($bundle && $entity_type) {
      $path_to_create = '/'.$entity_type.'/add/'.$bundle;
      $is_accessible_to_create = \Drupal::service('path.validator')->isValid($path_to_create);
      $data['is_accessible_to_create'] = $is_accessible_to_create;
      $data['path_to_create'] = $path_to_create;
    }

    dsm($data);
    return [
      '#theme' => 'backblock',
      '#data' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $config = $this->getConfiguration();
    
    $form['back2views'] = [
      '#type' => 'fieldset',
      '#title' => t('Back to List'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,  
    ];
    $form['back2views']['back_btn'] = [
      '#type' => 'fieldset',
      '#title' => t('Back Link/Button'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,  
    ];
    $form['back2views']['back_btn']['target_path'] = [
      '#type' => 'textfield',
      '#title' => t('Target Path'),
      '#description' => $this->t('The target path segment (URL of the views) to be evaluated, without leading slash.'),
      '#default_value' => $config['back2views']['back_btn']['target_path'] ?? '',
      '#required' => TRUE,
    ];
    $form['back2views']['back_btn']['target_link_title'] = [
      '#type' => 'textfield',
      '#title' => t('Link/Button title'),
      '#default_value' => $config['back2views']['back_btn']['target_link_title'] ?? 'Go Back',
      '#required' => TRUE,
    ];

    // @TODO: this is not saving the value somehow
    // $form['back2views']['add_create'] = [
    //   '#type' => 'checkbox',
    //   '#title' => t('Add Create new Node link/button'),
    //   '#default_value' => $config['back2views']['add_create'] ?? FALSE,
    //   //'#required' => TRUE,
    //   '#attributes' => [
    //     //define static name and id so we can easier select it
    //     // 'id' => 'add_create',
    //     'name' => 'field_add_create',
    //   ],
    // ];

    $form['back2views']['add_new'] = [
      '#type' => 'fieldset',
      '#title' => t('Create new item link/button'),
      '#collapsible' => TRUE,
      '#collapsed' => $config['back2views']['add_new']['create'] ?? FALSE,
      
      '#attributes' => [
        'id' => 'entity-selector',
        'name' => 'fieldset_entity_selector',
      ],
      '#states' => [
        //show this textfield only if the radio 'other' is selected above
        'visible' => [
          // Don't mistake :input for the type of field -- it's just a css selector.
          // You can always use :input or any other css selector here, no matter
          // whether your source is a select, radio or checkbox element.
          ':input[name="field_add_create"]' => ['checked' => TRUE],
        ],
      ],
    ];
    
    $all = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($all as $key => $value) {
       if($value instanceof ContentEntityType) {
        $entity_types[$key] = $value->getLabel(); 
       }
    }
    $form['back2views']['add_new']['entity_type'] = [
      '#type' => 'select',
      '#title' => t('Entity Type'),
      '#description' => t('Select entity type'),
      //'#required' => TRUE,
      '#options' => $entity_types,
      '#default_value' => $config['back2views']['add_new']['entity_type'] ?? '',
      '#ajax' => [
        'callback' => [$this, 'getBundles'],
        'event' => 'change',
        'method' => 'html',
        'wrapper' => 'bundle-to-update',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $options = [];
    if($config['back2views']['add_new']['entity_type'] != '') {
      $bundlesObj = \Drupal::service('entity_type.bundle.info')->getBundleInfo($config['back2views']['add_new']['entity_type']);
      foreach ($bundlesObj as $key => $value) {
         $options[$key] = $value['label'];
      }
    }

    $form['back2views']['add_new']['bundle'] = [
      '#title' => t('Bundle'),
      '#type' => 'select',
      '#description' => t('Select the bundles'),
      '#options' => $options,
      '#default_value' => $config['back2views']['add_new']['bundle'] ?? '',
      '#attributes' => [
        'id' => 'bundle-to-update'
      ],
      //'#multiple' => TRUE,
      '#validated' => TRUE,
    ];
    
    return $form;
  }
  
  public function getBundles(array &$element, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $value = $triggeringElement['#value'];
    $bundlesObj = \Drupal::service('entity_type.bundle.info')->getBundleInfo($value);
    foreach ($bundlesObj as $key => $value) {
       $options[$key] = $value['label'] ;
    }
    $wrapper_id = $triggeringElement["#ajax"]["wrapper"];
    $renderedField = '';
    foreach ($options as $key => $value) {
      $renderedField .= "<option value='".$key."'>".$value."</option>";
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#".$wrapper_id, $renderedField));
    return $response;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

  $this->setConfigurationValue('back2views', $form_state->getValue('back2views'));
    $this->messenger()->addMessage($this->t('The configuration for the back block has been set.'));
  }
  
  /**
   * {@inheritdoc}
   */
  // public function blockValidate($form, FormStateInterface $form_state) {
  //   if (empty($form_state->getValue('backblock_target_path'))) {
  //     $form_state->setErrorByName('backblock_target_path', $this->t('This field is required'));
  //   }
  // }
}