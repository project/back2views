CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers
 

INTRODUCTION
------------

A simple lightweight module to display back to list button/link.
You can place as many block as you want, using the Place block button on core block configuration (/admin/structure/block).

The module will check $_SERVER['referer'] for any query parameters is presented and try to keep them.

Optionally, you can add "Add New" button for a specific bundle you select for the user with the bundle create permission.

REQUIREMENTS
------------

TBD

RECOMMENDED MODULES
-------------------

 * [Markdown filter](https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configure the block as you would normally add block using "Place block" button, choose "Back to Views block"

 * Type the target path and the link/button title
 
 * Select "Add new content" option if needed.
 
 
MAINTAINERS
-----------

Current maintainers:
 * Jaesung Song - https://www.drupal.org/u/timestorys